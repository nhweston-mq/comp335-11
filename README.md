Repository for Group 11, COMP335 2019 S1.

## Getting started

To clone this repository:

```
git clone https://bitbucket.org/nhweston/comp335-11.git
```

### Running the client

1. In a command-line window, navigate to the `bin` directory.
2. Before running the client, start the server with `./ds-server`.
3. Run `./schd.sh`. (Type `./schd.sh -h` for further information.)

### Running the tests

1. In a command-line window, navigate to the `bin` directory.
2. Run `./test.sh`. (Type `./test.sh -h` for further information.)
3. Refer to the `logs` directory in the repository root with the results of the test.

### Building the JAR

1. Install [SBT](https://www.scala-sbt.org/download.html) (Scala build tool).
2. In a command-line window, navigate to the `bin` directory of this repository.
3. Run `./build.sh`. (Type `./build.sh -h` for further information.)
