ThisBuild/organization := "org.bitbucket.nhweston"
ThisBuild/scalaVersion := "2.12.8"
ThisBuild/scalacOptions := Seq (
    "-deprecation",
    "-feature",
    "-unchecked",
    "-Xcheckinit"
)

lazy val root = (project in file(".")).settings (
    name := "comp335-11",
    version := "0.1",
    libraryDependencies ++= Seq (
        "org.scala-lang.modules" %% "scala-xml" % "1.1.1"
    )
) .settings (
    assemblyJarName in assembly := "comp335_11_fat.jar",
    assemblyOutputPath in assembly := file(".") / "bin" / "comp335_11_fat.jar",
    mainClass in assembly := Some("org.bitbucket.nhweston.comp335_11.Main"),
    test in assembly := {}
)
