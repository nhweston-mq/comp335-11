package org.bitbucket.nhweston.comp335_11;

import org.bitbucket.nhweston.comp335_11.model.Job;
import org.bitbucket.nhweston.comp335_11.model.Server;

import java.util.List;

/**
 * Data access object for the scheduling algorithm to request system information.
 */
public interface Dao {

    /**
     * Fetches all resource information.
     *
     * @return a list of resource information entries for all servers.
     */
    List<Server> getResourceInfo();

    /**
     * Fetches resource information for all servers of a given type.
     *
     * @param typeId the tag of the server type to retrieve information for.
     * @return a list of resource information entries for the servers in question.
     */
    List<Server> getResourceInfo(String typeId);

    /**
     * Fetches resource information for all servers with the given resources available.
     *
     * @param numCores the minimum number of cores.
     * @param memory   the minimum amount of memory.
     * @param disk     the minimum amount of disk space.
     * @return a list of resource information entries for the servers in question.
     */
    List<Server> getResourceInfo(int numCores, int memory, int disk);

    /**
     * Fetches the list of jobs queued for the given server.
     *
     * @param typeId     the type ID of the desired server.
     * @param instanceId the instance ID of the desired server.
     * @return the job queue of the server, or <code>null</code> if the server does not exist.
     */
    List<Job> getJobList(String typeId, int instanceId);

}
