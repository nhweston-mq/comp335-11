package org.bitbucket.nhweston.comp335_11.scheduler;

import org.bitbucket.nhweston.comp335_11.model.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public interface JFirstFit extends Scheduler {

    @Override
    default Decision apply(Job job) {
        // TODO
        Server s1 = null;
        Server s2 = null;
        List<ServerType> typeList = new ArrayList<>(config().typesAsJava());
        typeList.sort(Comparator.comparingInt(ServerType::numCores));
        for (ServerType type : typeList)
            if (job.numCores() <= type.numCores() && job.memory() <= type.memory() && job.disk() <= type.disk()) {
            for (Server server : dao().getResourceInfo(type.id())) {
                if(s2 == null && server.state().equals(Server.Active$.MODULE$)){
                    s2 = server;
                }
                if (s1 == null && job.numCores() <= server.numCores() && job.memory() <= server.memory() && job.disk() <= server.disk()) {
                    s1 = server;
                }

            }
        }
        if(s1 == null) {
            assert s2 != null;
            return Decision$.MODULE$.apply(job.id(), s2.typeId(), s2.instanceId());
        } else {
            return Decision$.MODULE$.apply(job.id(), s1.typeId(), s1.instanceId());
        }
    }
}
