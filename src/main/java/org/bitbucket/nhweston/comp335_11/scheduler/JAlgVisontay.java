package org.bitbucket.nhweston.comp335_11.scheduler;

import org.bitbucket.nhweston.comp335_11.model.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public interface JAlgVisontay extends Scheduler {

    private Boolean fits(Server s, Job j) {
        return (j.numCores() <= s.numCores() && j.memory() <= s.memory() && j.disk() <= s.disk());
    }

    @Override
    default Decision apply(Job job) {
        Server s1 = null;
        Server s2 = null;
        Server s3 = null;

        List<ServerType> typeList = new ArrayList<>(config().typesAsJava());
        float rate = Float.MAX_VALUE;
        ServerType currentType = null;
        for (ServerType costType : typeList) {
            if (job.numCores() <= costType.numCores() && job.memory() <= costType.memory() && job.disk() <= costType.disk() && costType.hourlyRate().floatValue() <= rate) {
                rate = costType.hourlyRate().floatValue();
                currentType = costType;
            }
        }

        for (Server server : dao().getResourceInfo(currentType.id())) {
            if (s1 == null && server.state().equals(Server.Active$.MODULE$)) {
                s1 = server;
            }
            if (s2 == null && fits(server,job)
                    && (server.state().equals(Server.Inactive$.MODULE$))) {
                s2 = server;
            }
            if (s3 == null && fits(server,job)
                    && ((server.state().equals(Server.Active$.MODULE$)) || (server.state().equals(Server.Idle$.MODULE$)))) {
                s3 = server;
            }
        }

        if (s2 == null && s3 == null) {
            assert s1 != null;
            return Decision$.MODULE$.apply(job.id(), s1.typeId(), s1.instanceId());
        } else if (s3 == null) {
            return Decision$.MODULE$.apply(job.id(), s2.typeId(), s2.instanceId());
        } else {
            return Decision$.MODULE$.apply(job.id(), s3.typeId(), s3.instanceId());
        }
    }
}
