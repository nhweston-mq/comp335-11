package org.bitbucket.nhweston.comp335_11;

import java.io.IOException;
import java.util.Scanner;

/**
 * Allows the user to directly give commands to the simulator. Accessible by passing <code>-u</code> as a command line
 * parameter.
 */
public class UserDirected extends Client {

    public UserDirected(String dir) throws IOException {
        super(dir, null);
    }

    @Override
    public void run() throws IOException {
        Scanner scanner = new Scanner(System.in);
        while (socket.isConnected()) {
            String input = scanner.nextLine();
            send(input);
            System.out.println(receive());
        }
        socket.close();
    }

}
