package org.bitbucket.nhweston.comp335_11;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, IllegalArgumentException {
        if (args.length != 2) {
            throw new IllegalArgumentException("Expected two arguments");
        }
        String algId = args[0];
        String simDir = args[1];
        if (algId.equals("ud")) {
            new UserDirected(simDir).run();
        }
        else {
            new Client(algId, simDir).run();
        }
    }

}
