package org.bitbucket.nhweston.comp335_11;

import org.bitbucket.nhweston.comp335_11.model.*;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Client implements Dao {

    protected Socket socket;
    private InputStream in;
    private DataOutputStream out;
    private Scheduler scheduler;
    private String dir;
    private String algorithmName;

    public Client(String algorithmName, String dir) throws IOException {
        this.dir = dir;
        this.algorithmName = algorithmName;
        socket = new Socket("127.0.0.1", 8096);
        in = this.socket.getInputStream();
        out = new DataOutputStream(this.socket.getOutputStream());
    }

    public void run() throws IOException {
        send("HELO");
        receive();
        send("AUTH " + System.getProperty("user.name"));
        receive();
        Config config = Config.apply(readSystem());
        scheduler = Scheduler$.MODULE$.apply(algorithmName, config, this);
        send("REDY");
        loop();
    }

    public void loop() throws IOException {
        while(socket.isConnected()) {
            String input = receive();
            if(input.trim().equals("NONE")) {
                quit();
                return;
            }
            String[] arr = input.split("\\s+");
            int[] args = new int[arr.length-1];
            for (int i=1; i<arr.length; i++) {
                args[i-1] = Integer.parseInt(arr[i].trim());
            }
//            System.out.println(input);
            //System.out.println(Arrays.toString(arr));
            Job job = Job.apply(args[1], Job.Unknown$.MODULE$, args[0], args[2], args[3], args[4], args[5]);
            Decision decision = scheduler.apply(job);
            send("SCHD " + decision.jobId() + " " + decision.typeId() + " " + decision.instanceId());
            receive();
            send("REDY");
        }
    }

    public String readSystem() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(dir + "/system.xml"));
        StringBuilder builder = new StringBuilder();
        String line;
        while((line = reader.readLine()) != null) {
            builder.append(line);
        }
        reader.close();
        return builder.toString();
    }

    public void send(String msg) {
        try {
            out.write(msg.getBytes());
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String receive() {
        try {
            byte[] arr = new byte[128];
            in.read(arr);
            String string = new String(arr);
            return string;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void quit() throws IOException {
        send("QUIT");
        receive();
        socket.close();
        //TODO
    }

    @Override
    public List<Server> getResourceInfo() {
        return sendRESC("RESC All");
    }

    @Override
    public List<Server> getResourceInfo(String typeId) {
        return sendRESC("RESC Type " + typeId);
    }

    @Override
    public List<Server> getResourceInfo(int numCores, int memory, int disk) {
        return sendRESC("RESC Avail " + numCores + " " + memory + " " + disk);
    }

    @Override
    public List<Job> getJobList(String typeId, int instanceId) {
        List<Job> jobList = new ArrayList<>();
        String command = "LSTJ " + typeId + " " + instanceId;
        send(command);
        receive();
        send("OK");
        String line;
        while (!(line = receive().trim()).contains(".")) {
            send("OK");
            String[] params = line.split("\\s+");
            int jobId = Integer.parseInt(params[0].trim());
            Job.JobState state = Job$.MODULE$.getStateById(Integer.parseInt(params[1].trim()));
            int submitTime = Integer.parseInt(params[2].trim());
            int estimatedRuntime = Integer.parseInt(params[3].trim());
            int numCores = Integer.parseInt(params[4].trim());
            int memory = Integer.parseInt(params[5].trim());
            int disk = Integer.parseInt(params[6].trim());
            Job job = Job.apply(jobId, state, submitTime, estimatedRuntime, numCores, memory, disk);
            jobList.add(job);
        }
        return jobList;
    }

    public List<Server> sendRESC(String command){
        List<Server> serverList = new ArrayList<>();
        send(command);
        receive(); //DATA
        send("OK");
        String line;
        while(!(line = receive().trim()).contains(".")){
            send("OK");
            String[] params = line.split("\\s+");
            String typeId = params[0];
            int instanceId = Integer.parseInt(params[1]);
            Server.ServerState state = Server$.MODULE$.getStateById(Integer.parseInt(params[2]));
            int availableTime = Integer.parseInt(params[3]);
            short numCores = Short.parseShort(params[4]);
            int memory = Integer.parseInt(params[5]);
            int disk = Integer.parseInt(params[6]);
            Server s = new Server(typeId, instanceId, state, availableTime, numCores, memory, disk);
            serverList.add(s);
        }
        return serverList;
    }
}
