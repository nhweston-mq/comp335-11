package org.bitbucket.nhweston.comp335_11.scheduler.weston

import scala.collection.mutable

/**
 * A map that evaluates its entries lazily based on a given function.
 *
 * @param fn    the function used to evaluate map entries.
 * @tparam K    the type of keys stored by this map.
 * @tparam V    the type of values stored by this map.
 */
case class LazyMap[K, V] (fn: K => V) {

    /** The underlying mutable map. */
    private val map: mutable.Map[K, V] = mutable.Map.empty

    def apply (key: K) : V = {
        map.get (key) match {
            case Some(result) => result
            case None =>
                val result = fn (key)
                map += (key -> result)
                result
        }
    }

}
