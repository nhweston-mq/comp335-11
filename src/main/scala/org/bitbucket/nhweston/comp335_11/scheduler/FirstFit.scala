package org.bitbucket.nhweston.comp335_11.scheduler

import org.bitbucket.nhweston.comp335_11.model.Scheduler.{Companion, Impl}

object FirstFit extends Companion[JFirstFit] (new Impl (_, _) with JFirstFit)
