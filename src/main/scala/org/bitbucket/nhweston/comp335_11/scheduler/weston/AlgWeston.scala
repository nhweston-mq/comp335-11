package org.bitbucket.nhweston.comp335_11.scheduler.weston

import org.bitbucket.nhweston.comp335_11.model.Scheduler.{Companion, Impl}
import org.bitbucket.nhweston.comp335_11.model._

trait AlgWeston extends Scheduler {

    /**
     * A count of how many servers of each type are running at a particular step. This value is mapped by each `Step`
     * instance.
     */
    var serversOperating: Map[ServerType, Int] = {
        config.typeSeq.map {
            typ => typ -> 0
        } .toMap
    }

    override def apply (job: Job) : Decision = {
        val step = Step (config, dao, job, serversOperating)
        serversOperating = step.serversOperatingNext
        step.decision
    }

}

object AlgWeston extends Companion[AlgWeston] (new Impl (_, _) with AlgWeston) {

    /**
     * If the resources required for queued and running jobs exceeds that provided by operating servers by a factor of
     * `THRESHOLD`, the scheduler will seek to boot a new server.
     */
    val THRESHOLD: BigDecimal = 64

}
