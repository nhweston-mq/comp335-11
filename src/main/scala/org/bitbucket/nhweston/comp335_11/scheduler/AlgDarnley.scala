package org.bitbucket.nhweston.comp335_11.scheduler

import org.bitbucket.nhweston.comp335_11.model.Scheduler.{Companion, Impl}
import org.bitbucket.nhweston.comp335_11.model.Server.{Active, Idle, Unavailable, Unknown}
import org.bitbucket.nhweston.comp335_11.model._

trait AlgDarnley extends Scheduler {

    lazy val targetTypeId: String = config.types.maxBy(_._2.numCores)._1
    import scala.collection.convert.ImplicitConversionsToScala._

    def isFit (job: Job, typ: ServerType) : Boolean = isFit (job, typ.numCores, typ.memory, typ.disk)
    def isFit (job: Job, server: Server) : Boolean = isFit (job, server.numCores, server.memory, server.disk)
    def isFit (job: Job, numCores: Int, memory: Int, disk: Int) : Boolean = {
        job.numCores <= numCores && job.memory <= memory && job.disk <= disk
    }

    override def apply (job: Job) : Decision = {
        val typesFit: Seq[ServerType] = config.typeSeq.filter (isFit (job, _))
        typesFit.flatMap {
            typ => dao.getResourceInfo (typ.id) .filter {
                // Exclude servers that are unfit and those unavailable.
                server => isFit (job, server) && server.state != Unavailable && server.state != Unknown
            }
        } .partition {
            // Split servers into available and not.
            server => (server.availableTime < 0 && server.state == Active) || server.state == Idle
        } match {
            case (Nil, Nil) => Decision (job.id, targetTypeId, 0)
            case (Nil, servers) => Decision (job, servers.maxBy (_.numCores))
            case (servers, _) => Decision (job.id, targetTypeId, 0)
        }
    }


}

object AlgDarnley extends Companion[AlgDarnley] (new Impl (_, _) with AlgDarnley)
