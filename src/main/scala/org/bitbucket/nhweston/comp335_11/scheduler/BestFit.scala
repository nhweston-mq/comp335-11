package org.bitbucket.nhweston.comp335_11.scheduler

import org.bitbucket.nhweston.comp335_11.model.Scheduler.{Companion, Impl}
import org.bitbucket.nhweston.comp335_11.model.Server.{Active, Idle, Unavailable, Unknown}
import org.bitbucket.nhweston.comp335_11.model._

import scala.collection.convert.ImplicitConversionsToScala._

trait BestFit extends Scheduler {

    def isFit (job: Job, typ: ServerType) : Boolean = isFit (job, typ.numCores, typ.memory, typ.disk)
    def isFit (job: Job, server: Server) : Boolean = isFit (job, server.numCores, server.memory, server.disk)
    def isFit (job: Job, numCores: Int, memory: Int, disk: Int) : Boolean = {
        job.numCores <= numCores && job.memory <= memory && job.disk <= disk
    }
    def sorted (server: Server) : (Int, Int) = {
        return (server.numCores, server.availableTime)
    }

    override def apply (job: Job) : Decision = {
        val typesFit: Seq[ServerType] = config.typeSeq.filter (isFit (job, _))
        typesFit.flatMap {
            typ => dao.getResourceInfo (typ.id) .filter {
                // Excluding servers that are unfit or unavailable for use
                server => isFit (job, server) && server.state != Unknown && server.state != Unavailable
            }
        }


         match {
            case Nil => Decision (job.id, typesFit.minBy (_.numCores) .id, 0)
            case servers => Decision (job, servers.minBy (sorted))
        }
    }
}

object BestFit extends Companion[BestFit] (new Impl (_, _) with BestFit)
