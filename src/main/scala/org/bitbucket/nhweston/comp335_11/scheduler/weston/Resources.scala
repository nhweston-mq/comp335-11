package org.bitbucket.nhweston.comp335_11.scheduler.weston

import org.bitbucket.nhweston.comp335_11.model.{Job, Server, ServerType}

/**
 * Represents quantities of resources provided by a server or used by a job. This class provides convenient syntax for
 * arithmetic operations.
 */
case class Resources (numCores: BigDecimal, memory: BigDecimal, disk: BigDecimal) {

    def + (other: Resources) : Resources = {
        Resources (
            this.numCores + other.numCores,
            this.memory + other.memory,
            this.disk + other.disk
        )
    }

    def - (other: Resources) : Resources = this + -other

    def * (n: BigDecimal) : Resources = {
        Resources (
            numCores * n,
            memory * n,
            disk * n
        )
    }

    lazy val unary_- : Resources = Resources (-numCores, -memory, -disk)

    /**
     * Is `true` if and only if one or more resource quantities are negative.
     */
    lazy val isNegative : Boolean = (numCores < 0) || (memory < 0) || (disk < 0)

}

/**
 * Provides implicit conversions from jobs, servers, and server types to corresponding `Utilisation` instances.
 */
object Resources {

    import scala.language.implicitConversions

    def zero : Resources = Resources (0, 0, 0)

    implicit def apply (job: Job) : Resources = Resources (job.numCores, job.memory, job.disk)
    implicit def apply (server: Server) : Resources = Resources (server.numCores, server.memory, server.disk)
    implicit def apply (typ: ServerType) : Resources = Resources (typ.numCores, typ.memory, typ.disk)

}
