package org.bitbucket.nhweston.comp335_11.scheduler

import org.bitbucket.nhweston.comp335_11.model.Scheduler.{Companion, Impl}
import org.bitbucket.nhweston.comp335_11.model.{Decision, Job, Scheduler}

/**
 * Assigns each job to the first server of the type with the most cores.
 */
trait AllToLargest extends Scheduler {

    lazy val targetTypeId: String = config.types.maxBy(_._2.numCores)._1

    override def apply (job: Job) : Decision = {
        Decision (job.id, targetTypeId, 0)
    }

}

object AllToLargest extends Companion[AllToLargest] (new Impl (_, _) with AllToLargest)
