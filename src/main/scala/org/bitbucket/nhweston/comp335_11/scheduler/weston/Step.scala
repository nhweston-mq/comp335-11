package org.bitbucket.nhweston.comp335_11.scheduler.weston

import org.bitbucket.nhweston.comp335_11.Dao
import org.bitbucket.nhweston.comp335_11.model.Server.Inactive
import org.bitbucket.nhweston.comp335_11.model._
import org.bitbucket.nhweston.comp335_11.scheduler.weston.AlgWeston.THRESHOLD

/**
 * Encapsulates the logic behind a single scheduling decision for `AlgWeston`. An instance of this class is created for
 * every job submission.
 */
case class Step (config: Config, dao: Dao, job: Job, serversOperating: Map[ServerType, Int]) {

    import scala.collection.convert.ImplicitConversionsToScala._

    /** Lazily obtains resource information from the `Dao`. */
    lazy val resInfo: LazyMap[ServerType, Map[Int, Server]] = {
        LazyMap {
            typ => dao.getResourceInfo (typ.id) .map {
                server => server.instanceId -> server
            } .toMap
        }
    }

    /** Lazily obtains job lists from the `Dao`. */
    lazy val jobLists: LazyMap[(ServerType, Int), Seq[Job]] = {
        LazyMap {
            case (typ, iId) => dao.getJobList (typ.id, iId)
        }
    }

    /** Returns `true` if and only if a server of the given server type can accept the job. */
    def isFit (job: Job, typ: ServerType) : Boolean = isFit (job, typ.numCores, typ.memory, typ.disk)

    /** Returns `true` if and only if the server instance has sufficient resources to immediately handle the job. */
    def isFit (job: Job, server: Server) : Boolean = isFit (job, server.numCores, server.memory, server.disk)

    /** Returns `true` if and only if the given resource quantities are sufficient for the job. */
    def isFit (job: Job, numCores: Int, memory: Int, disk: Int) : Boolean = {
        job.numCores <= numCores && job.memory <= memory && job.disk <= disk
    }

    /** A list of server types that fit the job. */
    lazy val typesFit: Seq[ServerType] = config.typeSeq.filter (isFit (job, _))

    /** A list of server types that fit the job and have at least one operating instance. */
    lazy val typesFitAnyActive: Seq[ServerType] = typesFit.filter (serversOperating (_) > 0)

    /** A list of server types that fit the job and have at least one non-operating instance. */
    lazy val typesFitAnyInactive: Seq[ServerType] = typesFit.filter (typ => serversOperating (typ) < typ.limit)

    /** Servers that can immediately handle the job (after booting, if applicable). */
    lazy val serversAvailable: Seq[Server] = {
        typesFitAnyActive.flatMap {
            typ => resInfo (typ) .values .filter {
                server => server.state != Inactive && isFit (job, server)
            }
        }
    }

    /** The resource quantities currently required by all jobs running or queued on each server. */
    lazy val utils: Map[(ServerType, Int), Resources] = {
        serversOperating.flatMap {
            case (typ, numActive) => (0 until numActive) .map {
                i => (typ, i) -> jobLists ((typ, i)) .foldLeft (Resources.zero) (_ + _)
            }
        }
    }

    /** The resource quantities currently required by all jobs running or queued on the system. */
    lazy val utilSys: Resources = utils.values.foldLeft (Resources.zero) (_ + _)

    /** The total resource quantities currently provided by all operating servers. */
    lazy val capacity: Resources = {
        serversOperating.foldLeft (Resources.zero) {
            case (util, (typ, numActive)) => util + (Resources (typ) * numActive)
        }
    }

    /**
     * Is `true` if and only if the resource quantities required by all jobs exceeds that provided by all operating
     * servers by a factor of at least `THRESHOLD`.
     */
    lazy val isSystemAtCapacity : Boolean = ((capacity * THRESHOLD) - utilSys) .isNegative

    /** The average amount of memory required by a job per core required. */
    lazy val utilMemPerCore : BigDecimal = {
        if (utilSys.numCores == 0) 1
        else utilSys.memory / utilSys.numCores
    }

    /** The average amount of disk required by a job per core required. */
    lazy val utilDiskPerCore : BigDecimal = {
        if (utilSys.numCores == 0) 1
        else utilSys.disk / utilSys.numCores
    }

    /**
     * Returns the effective number of cores until a server reaches capacity from its current utilisation. The memory
     * and disk utilisation is converted to an effective number of cores based on the cores-memory and cores-disk
     * ratios in the current system utilisation.
     *
     * @param utilServer    the current utilisation of the server.
     * @param capServer     the capacity of the server.
     * @return              the effective number of cores until the server reaches capacity.
     */
    def coresToCap (utilServer: Resources, capServer: Resources) : BigDecimal = {
        val diff = capServer - utilServer
        val coresToCoreCap = diff.numCores
        val coresToMemCap = diff.memory / utilMemPerCore
        val coresToDiskCap = diff.disk / utilDiskPerCore
        Seq (coresToCoreCap, coresToMemCap, coresToDiskCap) .min
    }

    /**
     * Returns a decision assigning the job to an immediately available server by applying the best-fit criterion to
     * effective number of cores.
     */
    def selectAvailable : Decision = {
        Decision (job, serversAvailable .minBy (server => coresToCap (server, config.types (server.typeId))))
    }

    /**
     * Returns a decision assigning the job to the next non-operating instance of the server type fitting the job with
     * minimal cost.
     */
    def selectInactive (force: Boolean) : Decision = {
        val typ = typesFitAnyInactive.minBy (_.hourlyRate)
        Decision (job.id, typ.id, serversOperating (typ))
    }

    /**
     * Returns a decision assigning the job to the currently active server with minimal proportional workload.
     */
    def selectOperational : Decision = {
        typesFit.flatMap {
            typ => (0 until serversOperating (typ)) .map {
                i => {
                    val util = utils.getOrElse ((typ, i), Resources.zero)
                    val propCores = util.numCores / typ.numCores
                    val propMem = util.memory / typ.memory
                    val propDisk = util.disk / typ.disk
                    (typ, i) -> Seq (propCores, propMem, propDisk) .max
                }
            }
        } .minBy (_._2) match {
            case ((typ, iId), _) => Decision (job.id, typ.id, iId)
        }
    }

    /** The `Decision` forwarded to `AlgWeston`. */
    lazy val decision: Decision = {
        if (serversAvailable.nonEmpty) selectAvailable
        else if (typesFitAnyActive.isEmpty) selectInactive (true)
        else if (isSystemAtCapacity) {
            if (typesFitAnyInactive.isEmpty) selectOperational
            else selectInactive (false)
        }
        else selectOperational
    }

    /** The updated `Map` tabulating the number of operating servers of each type. */
    lazy val serversOperatingNext: Map[ServerType, Int] = {
        val typ = config.types (decision.typeId)
        val iId = decision.instanceId
        serversOperating.updated (typ, math.max (iId + 1, serversOperating (typ)))
    }

}
