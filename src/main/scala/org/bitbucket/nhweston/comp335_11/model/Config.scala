package org.bitbucket.nhweston.comp335_11.model

import org.bitbucket.nhweston.comp335_11.model.Config.JList

import scala.collection.JavaConverters
import scala.collection.immutable.ListMap
import scala.xml.XML

/**
 * The system configuration as specified in the `system.xml` file provided.
 *
 * @param types     the types of servers in this system.
 */
case class Config (types: ListMap[String, ServerType]) {

    /**
     * The server types as a `Seq` in insertion order. Provided for convenience here as `types.values` might not be
     * guaranteed to preserve insertion order.
     */
    lazy val typeSeq: Seq[ServerType] = types.toSeq.map (_._2)

    /**
     * The server types as a Java `List`.
     */
    def typesAsJava: JList[ServerType] = JavaConverters.seqAsJavaList (typeSeq)

}

object Config {

    type JList[T] = java.util.List[T]

    /**
     * Loads a configuration from an XML string.
     *
     * @param xml   the XML string.
     * @return      the system configuration.
     */
    def apply (xml: String) : Config = Config {
        (XML.loadString(xml) \ "servers" \ "server") .foldLeft (ListMap.empty[String, ServerType]) {
            case (map, n) => map + {
                val id: String = n \@ "type"
                id -> ServerType (
                    id,
                    (n \@ "limit") .toInt,
                    (n \@ "bootupTime") .toInt,
                    BigDecimal (n \@ "rate"),
                    (n \@ "coreCount") .toInt,
                    (n \@ "memory") .toInt,
                    (n \@ "disk") .toInt
                )
            }
        }
    }

}
