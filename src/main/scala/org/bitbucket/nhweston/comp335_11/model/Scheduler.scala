package org.bitbucket.nhweston.comp335_11.model

import org.bitbucket.nhweston.comp335_11._
import org.bitbucket.nhweston.comp335_11.scheduler._
import org.bitbucket.nhweston.comp335_11.scheduler.weston.AlgWeston

/**
 * Base trait for scheduling algorithm implementations.
 */
trait Scheduler extends (Job => Decision) {

    /** The system configuration. */
    val config: Config

    /** The data access object for information requests. */
    val dao: Dao

    /**
     * Schedules a job.
     *
     * @param job   the job to schedule.
     * @return      a scheduling decision for the given job.
     */
    override def apply (job: Job) : Decision

}

object Scheduler {

    def apply (algorithmId: String, config: Config, dao: Dao) : Scheduler = {
        algorithmId match {
            case "atl" => AllToLargest
            case "ff" => FirstFit
            case "bf" => BestFit
            case "wf" => WorstFit
            case "td" => AlgDarnley
            case "av" => AlgVisontay
            case "nw" => AlgWeston
        }
    } .apply (config, dao)

    /**
     * Abstract class for schedulers into which a scheduler implementation may be mixed.
     */
    abstract class Impl (override val config: Config, override val dao: Dao) extends Scheduler

    /**
     * Provides a pseudo-constructor for a scheduler implementation when extended by a companion object.
     *
     * @param factory   the pseudo-constructor function.
     * @tparam S        the type of scheduler implementation.
     */
    abstract class Companion[S <: Scheduler] (val factory: (Config, Dao) => S) {
        def apply (config: Config, dao: Dao) : S = factory (config, dao)
    }

}
