package org.bitbucket.nhweston.comp335_11.model

import org.bitbucket.nhweston.comp335_11.model.Server.ServerState

/**
 * Represents a server and its state at some point in time. Obtained via resource information requests.
 */
case class Server (
    typeId: String,
    instanceId: Int,
    state: ServerState,
    availableTime: Int,
    numCores: Int,
    memory: Int,
    disk: Int
)

object Server {

    sealed abstract class ServerState
    object Unknown extends ServerState
    object Inactive extends ServerState
    object Booting extends ServerState
    object Idle extends ServerState
    object Active extends ServerState
    object Unavailable extends ServerState

    def getStateById (id: Int) : ServerState = id match {
        case 0 => Inactive
        case 1 => Booting
        case 2 => Idle
        case 3 => Active
        case 4 => Unavailable
        case _ => Unknown
    }

}
