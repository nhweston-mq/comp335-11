package org.bitbucket.nhweston.comp335_11.model

import org.bitbucket.nhweston.comp335_11.model.Job.JobState

/**
 * Represents a job. Consumed by the scheduling algorithm. May be obtained via a job list request.
 */
case class Job (
    id: Int,
    state: JobState,
    submitTime: Int,
    estimatedRuntime: Int,
    numCores: Int,
    memory: Int,
    disk: Int
)

object Job {

    sealed abstract class JobState
    object Unknown extends JobState
    object Submitted extends JobState
    object Waiting extends JobState
    object Running extends JobState
    object Suspended extends JobState
    object Completed extends JobState
    object Failed extends JobState
    object Killed extends JobState

    def getStateById (id: Int) : JobState = id match {
        case 0 => Submitted
        case 1 => Waiting
        case 2 => Running
        case 3 => Suspended
        case 4 => Completed
        case 5 => Failed
        case 6 => Killed
        case _ => Unknown
    }

}
