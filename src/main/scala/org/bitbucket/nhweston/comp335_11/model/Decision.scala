package org.bitbucket.nhweston.comp335_11.model

/**
 * Represents a scheduling decision.
 */
case class Decision (jobId: Int, typeId: String, instanceId: Int)

object Decision {
    def apply (job: Job, server: Server) : Decision = Decision (job.id, server.typeId, server.instanceId)
}
