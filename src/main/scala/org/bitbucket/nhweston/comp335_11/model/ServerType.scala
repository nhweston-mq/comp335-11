package org.bitbucket.nhweston.comp335_11.model

/**
 * Represents a server type along with its specifications.
 */
case class ServerType (
    id: String,
    limit: Int,
    bootupTime: Int,
    hourlyRate: BigDecimal,
    numCores: Int,
    memory: Int,
    disk: Int
)
