## Design considerations and preliminaries

### Common data structures

All scheduler implementations extend from `Scheduler`, a _trait_ (akin to a Java _interface_, with some differences) with the following members:

- `config: Config`: The contents of the parsed `system.xml` file. In particular, a list of server types as a `ListMap`, preserving the order given in the XML file while allowing servers to be obtained by their ID.
- `dao: Dao`: The `Client` instance (responsible for communications) exposed via the `Dao` (data access object) interface, providing methods to obtain resource information.
- `apply (job: Job) : Decision`: The scheduling function itself, defined differently by each implementation. The `Client` parses a `JOBN` message to a `Job` which is forwarded to this method, which returns a `Decision`. The `Decision` is then converted to a `SCHD` invocation. Before returning, this method may invoke methods of `dao` to obtain resource information from the simulator.

All representations of system information are common and are included in the `model` package, including `Server`, `ServerType`, and `Job`. Most of these classes are _case classes_ (which can be thought of as akin to a struct in C).

### Facilitating scheduler implementations

Numerous classes exist to facilitate the creation of new scheduler implementations with as little duplicated code as possible, and to permit seamless development in both Scala and Java. All are contained in source files in:

- `src/main/scala/.../model/Scheduler.scala`: Defines common functionality for schedulers.
- `src/main/java/.../scheduler`: Contains scheduler implementations written in Java.
- `src/main/scala/.../scheduler`: Contains scheduler implementations written in Scala as well as pseudo-constructor companion objects (discussed further below).

Each implementation requires:

- A _trait_ (Scala) or _interface_ (Java) extending `Scheduler`, with the `apply` method overridden. These can be found in `JFirstFit.java`, `JBestFit.java`, and the trait declaration in `WorstFit.scala`.
- A _pseudo-constructor companion object_, each of which can be found in the Scala source file with the name of the respective algorithm.

The pseudo-constructor companion provides syntactic sugar for a scheduler to be instantiated with a particular implementation. For example, an `AllToLargest` scheduler can be instantiated with:

```
AllToLargest (config, dao)
```

This is syntactic sugar for:

```
val config0: Config = config
val dao0: Dao = dao
new AllToLargest () {
    override val config: Config = config0
    override val dao: Dao = dao0
}
```

In achieving this, the common need to provide a `config` and `dao` were factored out into the `Impl` abstract class. This mandates a constructor whereby the invoker provides the `config` and `dao` as constructor parameters. The implementation can then be _mixed in_ with the abstract class, for example:

```
new Impl (config, dao) with AllToLargest
```

Here, `Impl` provides `config` and `dao`, while `AllToLargest` provides the `apply` method, so this creates a valid concrete instance.

To simplify further, the `Companion` abstract class represents a _pseudo-constructor_ for an implementation `S`. The `Companion` constructor requires a _function_ that accepts a `config` and a `dao` and returns an instance of `S`. It then equips the extending object with an `apply` method that delegates to this function.

Creating one of these pseudo-constructor requires a single line of code:

```
object AllToLargest extends Companion[AllToLargest] (new Impl (_, _) with AllToLargest)
```

This allows an `AllToLargest` scheduler to be instantiated with `AllToLargest (config, dao)`.

### Obtaining resource information

Obtaining resource information is facilitated through the `Dao` interface, specifying three overloaded `getResourceInfo(...)` methods, with one corresponding to each of `RESC All`, `RESC Avail`, and `RESC Type`. There is also a `getJobList` method that has not been implemented as it is not yet needed. The `Dao` interface is implemented by `Client`, a single instance of which is responsible for communication between the simulator and the scheduler. The `Client` instance passes itself as a `Dao` to the `Scheduler` companion object, which instantiates the scheduler of the appropriate type. When processing a job, schedulers may invoke `Dao` methods on the `Client` instance to obtain resource information. The separation of concerns in this design allowed group members to work on developing algorithms _before_ the `Dao` methods were implemented.

The three `getResourceInfo` overloadings each are a single line, delegating to the `sendRESC` method which factors out common functionality. The overloadings convert the parameters to a string containing the `RESC` command which is passed to `sendRESC`.

The `sendRESC` method first instantiates an empty list of servers. It then sends the command, and after sending `OK` to `data` enters into a `while` loop. This loop receives a line from the simulator and checks whether this line is `.` (indicating the end of `DATA`), in which case the loop terminates and the server list is returned back to the scheduler. Otherwise, the line is parsed into a `Server` instance, which is appended to the list.
