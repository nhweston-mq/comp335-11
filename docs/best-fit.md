## Best-Fit Algorithm :

*Author: Travis Darnley*

The best-fit algorithm allocates data by finding the smallest available partition that can store it. This results in a minimised amount of wastage, allowing more data to be stored per partition. Using this algorithm with our server simulation will allow the minimum amount of servers to be used, making the result cheaper than the other algorithms.

The algorithm first removes the servers that are unfit or unavailable for use from the list, since they cannot be used to store data. The list is then split between the idle/available servers and the active servers. From these lists, the servers are assigned to a job by finding one with the smallest amount of available server space that can perform the job. By doing this, server space is maximised and the amount of servers needed is minimised.

The algorithm source file can be found here: src/main/scala/.../scheduler/BestFit.scala


