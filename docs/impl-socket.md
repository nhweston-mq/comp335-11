## Socket communication implementation

The Socket communication module was implemented using Java. Java was chosen mainly because if it's overall familiarity to the whole team.

- **Client.java**
	+ The `Client.java` class acts as an interface for the implemented 	Scheduler to receive and process job decisions. `org.bitbucket.nhweston.comp335_11.Client.java` sends and receives commands in order to retrieve jobs from the server, then applies methods to this raw data so to transform it into a form (object) the Scheduler can interperet.
- **Methods**
	+ `run()`
		* Performs inital setup and opens socket on localhost (8096). This includes server commands that only need to run once. Passes dumped `system.xml` file to implemented XML parser.
	+ `loop()`
		* Iterates through given jobs, extracts data to create `Job` object, which is passed to Scheduler to make scheduling decisions. Follows protocol closely, using blocking to ensure that messages have been received before the following message is sent to the server simulator.
	+ `readSystem()`
		* Accepts `system.xml` as a command line argument and writes to a String.
	+ `send()`
		* Writes given String message out as byte array.
	+ `receive()`
		* Receives messages in a byte array and returns as a String. Usage of `Java.io.InputStream.read()` blocks until input data is available, preventing concurrency issues.
	+ `quit()`
		* Issues `QUIT` command to the server. Closes the socket.

