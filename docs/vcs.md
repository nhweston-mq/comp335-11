## Getting started

1. Open Git Bash in the directory where you want the repository to be created. (Note that Git will create a new directory inside this directory, so if you want the root directory to be `C:/Documents/comp330-11`, open Git Bash in `C:/Documents`.
2. Clone this repository with `git clone https://<username>@bitbucket.org/nhweston/comp335-11.git`, where `<username>` is replaced with your username. (You may be prompted for authentication.)

## Common commands

- `git status`: Displays information regarding the current working tree.

### Pushing changes
- `git add <file>`: Stages `<file>`.
- `git add .`: Stages _all_ local files (both those that have been changed as well as new files) excluding those in `.gitignore`.
- `git add -u`: Stages all updated files (i.e. existing files that have been modified, but not new ones).
- `git commit -a -m "<message>"`: Commits all staged files with message `<message>`.
- `git push origin master`: Pushes the commit to this repository.

### Pulling changes
- `git pull`: Pulls the most recent commit (on the current branch).
- `git fetch`: Retrieves changes from the most recent commit with attempting to merge them with local files.

### Branches
- `git checkout <branch>`: Switches to branch `<branch>`.
- `git branch <branch>`: Creates a new branch with name `<branch>`.
- `git merge <branch>`: Attempts to merge `<branch>` into the current branch.

### Other
- `git rm <file>`: Removes `<file>`. (**WARNING**: This will _delete_ the file from the local workspace too!)
- `git rm <file> --cached`: Removes `<file>` _without_ deleting it from the local workspace.
- `git reset HEAD --hard`: The panic button. (**WARNING**: Assume that any local changes will be wiped when using this command.)

## Example use cases

### Making changes

1. Use `git status` to ensure you have the most recent commit.
2. If you only wish to commit changes to tracked files (i.e. "Changes not staged for commit"), use `git add -u`. If you wish to commit changes to all files, including untracked files, use `git add .`. Otherwise, use `git add <file>` to stage only the files you intend to commit.
3. Create a commit using `git commit -a -m "<message>"`.
4. Push the commit using `git push origin master`.