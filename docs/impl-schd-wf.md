## Worst-fit

*Author: Nicholas Weston*

The worst-fit implementation is written in Scala and can be found in `src/main/scala/.../scheduler/WorstFit.scala`.

Three utility methods with the name `isFit` are defined, which are used to check if a server _instance_ or server _type_ is _fit_ for a given job. By "fit", in the predicate sense, we mean that the number of cores, memory, and disk required by the job are less than or equal to that of a particular server or server type. A server _instance_ is fit for the job if it has the available resources for the job at a given time, whereas a server _type_ is fit if the total resources in any instance of that type fits the job.

The scheduling function (at line 17) starts by first excluding server types that cannot fit the job (line 18), since a server instance will _never_ be fit for a job if and only if its type is unfit the job. Indeed, the simulator will refuse a scheduling decision that assigns a job to such a server instance. The server types that _are_ fit are stored in configuration order in a `Seq` (akin to a `List` in Java).

The `typeSeq` is the list of server types, as parsed from `system.xml`. The `filter` method is a higher-order function that removes all elements of the `Seq` that do not satisfy a given predicate (i.e. a function returning a `Boolean`). The expression `isFit (job, _)` is syntactic sugar for a lambda expression equivalent to `t => isFit (job, t)`.

Subsequently, the types that _are_ fit for the job are expanded into their individual server instances with current resource information obtained from the simulator, and unfit server _instances_ are excluded in the process. Lines 19-24 are a _flat map_, where each element of a collection is transformed to a collection, and all those resulting collections are concatenated together. In this case, each fit server type is converted into a list of resource information (line 20) for _fit_ server instances of the given type.

The specification gives three cases for how the worst-fit decision should be made:

- *Case 1*: If the list of immediately available fit servers is non-empty, apply worst-fit to this list.
- *Case 2*: If the list of fit servers is non-empty, but there are no immediately available fit servers, apply worst-fit to this list.
- *Case 3*: If there are no fit servers, apply worst-fit to the list of server types and schedule the job to instance 0 of the resulting type.

A server is considered immediately available if it is active and the available time returned by `RESC All` or `RESC Type` is negative, or if the server is idle. Note that the mention of servers "available in a short definite amount of time" in the specification is redundant, as active servers that may fit the job some time in the future have already been filtered out on lines 20-23.

To find which case applies, lines 24-27 partition the list of fit servers into those that are immediately available and those that are not. Then, a `match` block (lines 27-31) finds which case applies and takes appropriate action:

- Line 28: If both partitions are empty (denoted `Nil` in Scala), then clearly there are no fit servers, so worst-fit is applied to type (Case 3).
- Line 29: If the partition containing immediately available servers is empty, apply worst-fit to the remaining servers (Case 2), i.e. those in the other partition.
- Line 30: If the partition containing immediately available servers is _non_-empty, apply worst-fit only to these servers (Case 1).

It is necessary for these cases to be in reverse order because pattern matching is _greedy_, e.g. a pair of empty lists will match `(Nil, servers)`.  (Note that `servers` is just an identifier to associate to a matched object).

With all unfit servers filtered out, finding the _worst-fit_ reduces to finding the fit server with the maximum number of cores, hence the `maxBy (_.numCores)` invocations. The result is passed to a `Decision` pseudo-constructor and is returned to be communicated to the simulator.
