## First-fit

*Author: Alexander Visontay*

The first-fit algorithm allocates data (jobs) to the first server of which the defined 'fit' is satisfied. This approach to allocation is achieved by iterating through the servers in sorted order (by size) and testing the jobs against the server parameters to ensure proper fit. When the correct fit is found, the job is allocated to that particular server. The algorithm has no regard for the size of the server, as long as the minimum 'fit' is satisfied. This iterative approach can be seen as less efficient when compared to `best-fit`. First-fit could also be considered as wasteful due to the remaining unused memory after allocation.