# Demo Procedure

## Getting started

To clone this repository:

```
git clone https://bitbucket.org/nhweston/comp335-11.git
```

### Compilation from source

1. Open a Linux VM.
2. Ensure that SBT (Scala Build Tool), Scala binaries and compatible JDK are installed.
3. Navigate to `/bin` folder and execute `./build.sh` to build from Scala and Java sources.

### Running the client & server

1. Open a Linux VM (must have JRE (Java Runtime Environment) installed)
3. Open a terminal session and navigate to the `/bin` directory.
4. Execute `./test.sh -a <atl/ff/bf/wf> -c ../data/ds-sim-tests/` to begin suite of tests.
5. Execute with `-h` option for usage.
