## System Overview:
**HIGH LEVEL DESCRIPTION OF THE CLIENT-SIDE SIMULATOR**

The client-side simulator will connect to the server and receive the jobs to be distributed. It will then schedule them all to the first of the largest server type given in the config XML file. All processes of the client will be logged to ensure than the system is operating properly. 

**CONSIDERATIONS**
We have used both Java and Scala due to the ability for both languages to be used together. This allows us to create seperate parts of the project in these languages as relevant. This however would result in all Scala dependancies being stored in the produced JAR file, so these would need to be removed to ensure small file sizes. 

**CONSTRAINTS**
Due to Scala running as a Java program, it will be slower than pure Java code. However, the slowdown in this project is so small as to be insignificant compared to the advantages of using Scala. Additionally, the file size constraint of combining the Java and Scala code is eliminated by removing the unused Scala dependencies, making the JAR file's size as minimal as possible.

**HIGH LEVEL DESCRIPTION OF SERVER SIDE SIMULATOR**

The server-side simulator will send the jobs to the client, and recieve the results back after processing. It functions as a example of a potential data set that could be sent to the client. This will allow us to test a variety of data sets repeatedly and reliably, to ensure consistent results from our client.
