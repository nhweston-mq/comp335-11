## Introduction:
In this stage of the project, we are designing three different scheduling algorithms, based upon the memory allocation of operating systems. These are First Fit (FF), Best Fit (BF) and Worst Fit (WF).

FF is when data is assigned to the first available memory partition. This allows data to be allocated the fastest of the three algorithms, as searching will be completed quickly.

BF allocates the data by finding the smallest available partition that can store the data. While this is slower than FF, it allows more data to be stored since the algorithm allocates memory so that wastage is minimised.

WF allocates data by finding the largest available partition. This will create major memory wastage, which may prevent larger amounts of data from being stored at a later stage.

While these memory allocation methods are normally used on operating systems, we can recreate these for use with our servers to simulate the storage of data. This will allow us to determine the usefulness of these algorithms for our server allocation program.

