## Introduction

This project is about discrete event simulation of distributed systems. By simulating a distributed system, we can determine what its real-world activity would be without having to set up clients and servers, allowing quick testing and debugging.

The aim of this project is to develop a �Job Scheduler� system for Cloud Data Centres. This system will determine the most cost-efficient way to allocate computer resources for a distributed computer system, allowing the data centre to correctly assign the resources that the client would need.

In this stage of the project, we are creating a client-side simulator that will connect to the server-side simulator, receive the jobs and schedule them all to the largest server type. This will allow us to test our client-server system and ensure that data is sent and received correctly on both ends.
