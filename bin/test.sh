DEF_ALGORITHM_ID="atl"
DEF_CONFIG_DIR="../data/simple-tests"
DEF_DIFF_NAME="diff.log"
DEF_LOG_DIR="../logs"

usage() {
    echo   ""
    echo   "    Usage: $0 [options]"
    echo   ""
    echo   "    Options:"
    echo   "        -a <algorithm-id>   Runs the tests with the given algorithm."
    echo   "        -c <config-dir>     Runs a test for each config in the given directory."
    echo   "        -d <diff-name>      Sets the file name for the diff log."
    echo   "        -l <log-dir>        Sets the directory the logs are written to."
    echo   ""
    echo   "    Algorithms available:"
    echo   "        atl     All-to-largest"
    echo   "        ff      First-fit"
    echo   "        bf      Best-fit"
    echo   "        wf      Worst-fit"
    echo   ""
    exit
}

algorithmId=""
configDir=""
diffName=""
logDir=""
while [ "$1" != "" ]; do
    case $1 in
        -a  )   if [ "$algorithmId" != "" ]; then usage; fi
                shift
                case $1 in
                    "atl"|"ff"|"bf"|"wf") algorithmId="$1";;
                    *) usage;;
                esac
                shift;;
        -c  )   if [ "$configDir" != "" ]; then usage; fi
                shift
                if [ -d $1 ]; then
                    configDir="$1"
                else
                    echo "Directory \"$1\" does not exist"
                    exit
                fi
                shift;;
        -d  )   if [ "$diffName" != "" ]; then usage; fi
                shift
                if [ "$1" == "" ]; then usage; fi
                diffName="$1"
                shift;;
        -l  )   if [ "$logDir" != "" ]; then usage; fi
                shift;
                if [ "$1" == "" ]; then usage; fi
                logDir="$1"
                shift;;
        *   )   usage
    esac
done

if [ "$algorithmId" == "" ]; then algorithmId=$DEF_ALGORITHM_ID; fi
if [ "$configDir" == "" ]; then configDir=$DEF_CONFIG_DIR; fi
if [ "$diffName" == "" ]; then diffName=$DEF_DIFF_NAME; fi
if [ "$logDir" == "" ]; then logDir=$DEF_LOG_DIR; fi

if [ ! -d $logDir ]; then
    mkdir $logDir
fi

if [ -f $logDir/$diffName ]; then
	rm $logDir/*
fi

trap "kill 0" EXIT

for filePath in $configDir/*.xml; do
	fileName=`basename $filePath`
	echo "$fileName"
	echo "Running the reference implementation..."
	sleep 1
    ./ds-server -c $filePath -v brief > $logDir/$fileName.ref.log&
    sleep 1
    if [ "$algorithmId" == "atl" ]; then
        ./ds-client
    else
        ./ds-client -a "$algorithmId"
    fi
	echo "Running our implementation..."
	sleep 2
    ./ds-server -c $filePath -v brief > $logDir/$fileName.your.log&
	sleep 1
	./schd.sh -a "$algorithmId"
	sleep 1
	diff $logDir/$fileName.ref.log $logDir/$fileName.your.log > $logDir/temp.log
	if [ -s $logDir/temp.log ]; then
		echo Failed.
	else
		echo Passed.
	fi
	echo "*** *** ***"
	sleep 1
	cat $logDir/temp.log >> $logDir/$diffName
done

echo "Finished testing. See $logDir for results."
