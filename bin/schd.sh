usage()
{
    echo   ""
    echo   "    Usage: $0 [-a <algorithm_id> | -u] [-d <xml_dir>]"
    echo   "        -a <algorithm_id>   Runs the scheduler with the given algorithm."
    echo   "        -d <xml_dir>        Specifies the directory to which the system XML file is written."
    echo   "        -u                  Runs a user-directed scheduler."
    echo   ""
    echo   "    Algorithms available:"
    echo   "        atl     All-to-largest"
    echo   "        ff      First-fit"
    echo   "        bf      Best-fit"
    echo   "        wf      Worst-fit"
    echo   "        nw      Nicholas Weston's Stage 3 algorithm."
    echo   ""
    exit
}

alg=""
dir=""
while [ "$1" != "" ]; do
    case $1 in
        -a  )   if [ "$alg" != "" ]; then usage; fi
                shift
                alg="$1"
                shift;;
        -u  )   if [ "$alg" != "" ]; then usage; fi
                alg="ud"
                shift;;
        -d  )   if [ "$dir" != "" ]; then usage; fi
                shift
                if [ -d $1 ]; then
                    dir="$1"
                else
                    echo "Directory \"$1\" does not exist."
                    exit
                fi
                shift;;
        *   )   usage
    esac
done

if [ "$alg" == "" ]; then
    alg="atl"
fi

if [ "$dir" == "" ]; then
    dir="."
fi

java -jar comp335_11.jar "$alg" "$dir"