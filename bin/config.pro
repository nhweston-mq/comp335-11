-ignorewarnings
-injars comp335_11_fat.jar
-outjars comp335_11.jar
-printmapping mapping.txt

-dontoptimize
-dontusemixedcaseclassnames
-useuniqueclassmembernames
-overloadaggressively
-repackageclasses ''
-allowaccessmodification
-useuniqueclassmembernames

-keepattributes Signature,*Annotation*

-keep public class org.bitbucket.nhweston.comp335_11.Main {
    public static void main(java.lang.String[]);
}

-keepnames class scala.xml.**

-keepclassmembernames class scala.xml.** {
    *;
}

-keepclassmembers class * {
    ** MODULE$;
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
