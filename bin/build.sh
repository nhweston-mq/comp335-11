shrink=0

usage()
{
    echo ""
    echo "    Usage: build [-s]"
    echo "        -s: Shrink the JAR."
    echo ""
    exit
}

renamefatjar()
{
    mv comp335_11_fat.jar comp335_11.jar
    exit
}

# Read command-line arguments:

if [ "$1" != "" ]; then
    case $1 in
        -s  )   shrink=1
                ;;
        *   )   usage
                ;;
    esac
fi

if [ "$2" != "" ]; then
    usage
fi

# Delete existing JARs:

if [ -f comp335_11_fat.jar ]; then
    rm comp335_11_fat.jar
fi

if [ -f comp335_11.jar ]; then
    rm comp335_11.jar
fi

# Run SBT assembly:

cd ..
sbt assembly
cd bin

# Check that the JAR was emitted:

if [ ! -f comp335_11_fat.jar ]; then
    echo "It seems SBT assembly did not emit a JAR. Is SBT installed?"
    exit
fi

# Should we shrink or simply rename?

if [ $shrink -eq 0 ]; then
    renamefatjar
fi

if [ ! -f proguard.jar ]; then
    echo "You must include proguard.jar in this directory to shrink the JAR."
    echo "Download ProGuard here: https://sourceforge.net/projects/proguard/"
    renamefatjar
fi

if [ "$JAVA_HOME" == "" ]; then
    echo "Set JAVA_HOME to the root directory of your Java installation and export it."
    renamefatjar
fi

if [ ! -f "$JAVA_HOME/lib/rt.jar" ]; then
    echo "It seems $JAVA_HOME does not contain a Java installation."
    renamefatjar
fi

java -jar proguard.jar @config.pro -verbose -libraryjars "\"${JAVA_HOME}\""
